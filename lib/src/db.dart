import 'dart:async';

import 'package:database_interface/database_interface.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;
import 'package:mongo_database/src/collection.dart';

class MongoDatabase implements NoSqlDatabaseInterface {
  static MongoDatabase _inMemoryDb;

  mongo.Db _db;
  bool _isOpen = false;

  factory MongoDatabase(String url) =>
      _inMemoryDb ??= MongoDatabase._(mongo.Db(url));

  MongoDatabase._(this._db);

  @override
  bool get isOpen => _isOpen;

  @override
  Future<void> close() async {
    _isOpen = false;
    await _db.close();
  }

  @override
  MongoCollection collection(String collectionName) =>
      MongoCollection(_db.collection(collectionName));

  @override
  Future<void> open() async {
    _isOpen = true;
    await _db.open();
  }
}
