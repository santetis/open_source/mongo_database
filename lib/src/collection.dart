import 'dart:async';

import 'package:database_interface/database_interface.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;

class MongoCollection implements CollectionInterface {
  mongo.DbCollection _collection;

  MongoCollection(this._collection);

  @override
  Future<void> delete(Map<String, dynamic> selector) async {
    final selectorBuilder = _buildQuery(selector);
    await _collection.remove(selectorBuilder);
  }

  @override
  Future<void> drop() async {
    await _collection.drop();
  }

  @override
  Stream<Map<String, dynamic>> find([Map<String, dynamic> selector]) {
    final _selector = _transformDatabaseIdInterfaceToObjectId(selector);
    final selectorBuilder = _buildQuery(_selector);
    return _collection.find(selectorBuilder).map((doc) {
      if (doc.containsKey('_id') && doc['_id'] is mongo.ObjectId) {
        final objectId = doc['_id'] as mongo.ObjectId;
        doc['_id'] = DatabaseId(objectId.toHexString());
      }
      return doc;
    });
  }

  @override
  Future<Map<String, dynamic>> findOne(Map<String, dynamic> selector) async {
    final _selector = _transformDatabaseIdInterfaceToObjectId(selector);
    final selectorBuilder = _buildQuery(_selector);
    final data = await _collection.findOne(selectorBuilder);
    if (data == null) {
      return null;
    }
    if (data.containsKey('_id') && data['_id'] is mongo.ObjectId) {
      final objectId = data['_id'] as mongo.ObjectId;
      data['_id'] = DatabaseId(objectId.toHexString());
    }
    return data;
  }

  @override
  Future<void> insert(Map<String, dynamic> document) =>
      _collection.insert(document);

  @override
  Future<void> update(
      Map<String, dynamic> selector, Map<String, dynamic> document) async {
    final _selector = _transformDatabaseIdInterfaceToObjectId(selector);
    final _document = _transformDatabaseIdInterfaceToObjectId(document);
    final selectorBuilder = _buildQuery(_selector);
    await _collection.update(selectorBuilder, _document);
  }

  mongo.SelectorBuilder _buildQuery(Map<String, dynamic> selector) {
    if (selector == null) {
      return null;
    }
    var selectorBuilder = mongo.where;
    dynamic value;

    for (final key in selector.keys) {
      value = selector[key];
      selectorBuilder = selectorBuilder.raw(<String, dynamic>{key: value});
    }
    return selectorBuilder;
  }

  Map<String, dynamic> _transformDatabaseIdInterfaceToObjectId(
      Map<String, dynamic> selector) {
    if (selector?.containsKey('_id') == true) {
      if (selector['_id'] is DatabaseId) {
        final dbId = selector['_id'] as DatabaseId;
        selector['_id'] = mongo.ObjectId.fromHexString(dbId.id);
      } else if (selector['_id'] is String) {
        final id = selector['_id'] as String;
        selector['_id'] = mongo.ObjectId.fromHexString(id);
      }
    }
    return selector;
  }
}
